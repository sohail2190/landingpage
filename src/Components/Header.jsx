import React,{useRef} from 'react'
import './Header.css'

const Header = ()=>{
    const navRef = useRef();

    const showNavbar = ()=>{
        navRef.current.classList.toggle('nav_open')
    }

    return(<>
        <header>    
            <div className='container'>
                <nav>
                    <ul ref={navRef}> 
                        <li><a href='#'>Home</a></li>
                        <li><a href='#'>About</a></li>
                        <li><a href='#'>Contact</a></li>
                        <li><a href='#'>Music</a></li>
                    </ul>
                    <div  className='toggler_icon' onClick={showNavbar}>
                    <i className="fa-solid fa-bars"></i>
                    </div>
                </nav>
                <div className='search-box'>
                    <input type='text' placeholder='Search Your Music ....' />
                </div>
                    {/* ------------------------------------------------- */}
                    <div className='circle'></div>
                    <div className='circl2'></div>
                    {/* ------------------------------------------------- */}
                <div className='header_content'>
                    <h5>Find Your Needs</h5>
                    <h1>Minimal Volume</h1>
                    <p>lorem ipsumtext is the dummy text to <br/>
                    show just header_content
                    it will help you  <br/> in doing whting</p>
                    {/* ------------------------------------------------- */}
                    <div className='Social_link'>
                        <ul>
                            <li><a href='#'> <i className="fa-brands fa-facebook-f"> </i></a></li>
                            <li><a href='#'> <i className="fa-brands fa-youtube"> </i></a></li>
                            <li><a href='#'> <i className="fa-brands fa-instagram"> </i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className='l_Half_circle'></div>
            <div className='r_Half_circle'></div>
        </header>
        
    </>)
}

export default Header;